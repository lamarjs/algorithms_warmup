/*
 * Copyright (C) 2017 Lamar J. Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package diagonal_difference;

import java.util.Scanner;

/**
 *
 * @author lsmith
 * 
 * This is solution for a code challenge on hackerrank: 
 * https://www.hackerrank.com/challenges/diagonal-difference
 * 
 * This class should be uploaded as a solution to the challenge. The package
 * declaration above should be removed as the submission engine does not support
 * them for uploaded answers. Alternatively, this code can just be copied and
 * pasted into the submission box, but take care to select the appropriate
 * language, Java 8 (or Java 7 in most cases), when submitting this way.
 */
public class Main {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int[][] matrix = getMatrix(stdin);
        System.out.println(Math.abs(getDiagonalDifference(matrix)));
    }

    private static int[][] getMatrix(Scanner stdin) {
        int length = stdin.nextInt();
        int[][] matrix = new int[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                matrix[i][j] = stdin.nextInt();
            }
        }
        return matrix;
    }

    private static int getDiagonalDifference(int[][] matrix) {
        return primarySum(matrix) - secSum(matrix);
    }

    private static int primarySum(int[][] matrix) {
        int sum = 0;
        int j = matrix.length - 1;
        for (int i = 0; i < matrix.length; i++, j--) {
            sum += matrix[i][j];
        }
        return sum;
    }

    private static int secSum(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            sum += matrix[i][i];
        }
        return sum;
    }
}
