Challenges from https://www.hackerrank.com/domains/algorithms/warmup

Each package in the src folder of this repo represents one of the warmup challenges in the algorithms section of hackerrank.com. The prompt for each challenge is noted in the comments above each package's Main class along with the details on how to run it on hackerrank.

# Notes #
One of my primary goals (especially given the relative ease of the challenges in this section) was to improve the way that I approach problems and organize code while doing so. I focused on naming functions in the main method that outlined the general requirements of the challenge and then defined those functions afterward.

What I observed when forcing myself to approach the problem this way is that I was better able to understand the requirements of the challenge through the creation of the high level code in the main method. The secondary benefit was that I could narrow my focus to the implementation of tiny slices of the overall program at the end. Unsurprisingly, filling main() with functions and banishing pesky logic to the definitions of those functions where possible is very fulfilling. This also makes testing and debugging significantly easier.