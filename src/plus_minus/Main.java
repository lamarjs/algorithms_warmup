/*
 * Copyright (C) 2017 Lamar J. Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package plus_minus;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author lsmith
 *
 * This is solution for a code challenge on hackerrank:
 * https://www.hackerrank.com/challenges/plus-minus
 *
 * This class should be uploaded as a solution to the challenge. The package
 * declaration above should be removed as the submission engine does not support
 * them for uploaded answers. Alternatively, this code can just be copied and
 * pasted into the submission box, but take care to select the appropriate
 * language, Java 8 (or Java 7 in most cases), when submitting this way.
 */
public class Main {

    public static void main(String[] args) {

        Scanner stdin = new Scanner(System.in);
        int length = stdin.nextInt();

        System.out.println(analyzeSigned(stdin, length));

    }

    private static String analyzeSigned(Scanner stdin, int length) {
        Integer positive = 0;
        Integer negative = 0;
        Integer zero = 0;

        for (int i = 0; i < length; i++) {
            int element = stdin.nextInt();
            if (element > 0) {
                positive++;
            } else if (element < 0) {
                negative++;
            } else {
                zero++;
            }
        }

        double p = positive.doubleValue() / length;
        double n = negative.doubleValue() / length;
        double z = zero.doubleValue() / length;

        DecimalFormat df = new DecimalFormat("#.000000");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(p) + "\n" + df.format(n) + "\n" + df.format(z);
    }
}
