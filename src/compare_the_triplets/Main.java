/*
 * Copyright (C) 2017 Lamar J. Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package compare_the_triplets;

import java.util.Scanner;

/**
 *
 * @author lsmith
 * 
 * This is solution for a code challenge on hackerrank: 
 * https://www.hackerrank.com/challenges/compare-the-triplets
 * 
 * This class should be uploaded as a solution to the challenge. The package
 * declaration above should be removed as the submission engine does not support
 * them for uploaded answers. Alternatively, this code can just be copied and
 * pasted into the submission box, but take care to select the appropriate
 * language, Java 8 (or Java 7 in most cases), when submitting this way.
 */
public class Main {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int[] a = nextTriplet(stdin);
        int[] b = nextTriplet(stdin);
        stdin.close();
        System.out.println(compareTriplets(a, b));        
    }

    public static int[] nextTriplet(Scanner stdin) {
        int[] triplet = new int[3];
        for (int i = 0; i < 3; i++) {
            triplet[i] = stdin.nextInt();
        }
        return triplet;
    }

    public static String compareTriplets(int[] a, int[] b) {
        int aScore = 0;
        int bScore = 0;
        for (int i = 0; i < 3; i++) {
            if (a[i] > b[i]) {
                aScore += 1;
            }
            
            if (a[i] < b[i]) {
                bScore += 1;
            }
        }
        return aScore + " " + bScore;
    }
}
