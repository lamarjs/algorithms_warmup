/*
 * Copyright (C) 2017 Lamar J. Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package circular_array_rotation;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import org.junit.Assert;
import static java.util.Collections.rotate;

/**
 *
 * @author lsmith
 */
public class MainTest {
    
    public MainTest() {
    }

    /**
     * Test rotate function in collections.
     */
    @Test
    public void testRotateDist1() {
        System.out.println("rotate");
        LinkedList<Integer> testList= new LinkedList<>(Arrays.asList(0, 1, 2, 3, 4));
        LinkedList<Integer> expected= new LinkedList<>(Arrays.asList(4, 0, 1, 2, 3));
        rotate(testList, 1);
        Assert.assertEquals(expected, testList);        
    }
    
    /**
     * Test rotate function in collections.
     */
    @Test
    public void testRotateDist2() {
        System.out.println("rotate2");
        LinkedList<Integer> testList= new LinkedList<>(Arrays.asList(0, 1, 2, 3, 4));
        LinkedList<Integer> expected= new LinkedList<>(Arrays.asList(3, 4, 0, 1, 2));
        rotate(testList, 2);
        Assert.assertEquals(expected, testList);        
    }
    
}
