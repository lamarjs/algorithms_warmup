/*
 * Copyright (C) 2017 Lamar J. Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package time_conversion;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.Scanner;

/**
 *
 * @author lsmith
 *
 * This is solution for a code challenge on hackerrank:
 * https://www.hackerrank.com/challenges/time-conversion
 *
 * This class should be uploaded as a solution to the challenge. The package
 * declaration above should be removed as the submission engine does not support
 * them for uploaded answers. Alternatively, this code can just be copied and
 * pasted into the submission box, but take care to select the appropriate
 * language, Java 8 (or Java 7 in most cases), when submitting this way.
 */
public class Main {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String timeString = stdin.next();       
        
        LocalTime time = getLocalTime(timeString);
        String period = getPeriod(timeString);
          
        System.out.println(getMillitaryTime(period, time).format(DateTimeFormatter.ISO_LOCAL_TIME));
    }

    private static LocalTime getMillitaryTime(String period, LocalTime time) {
        if (period.equalsIgnoreCase("am")) {
            if (time.getHour() == 12) {
                return time.minusHours(12);
            }
        } else if (period.equalsIgnoreCase("pm") && time.getHour() != 12) {
            return time.plusHours(12);
        }
        return time;
    }

    private static String getPeriod(String timeString) {
        return timeString.substring(timeString.length() - 2, timeString.length());
    }

    private static LocalTime getLocalTime(String timeString) {
        if (timeString.charAt(1) == ':') {
            timeString = "0" + timeString;
        }
        return LocalTime.parse(timeString.substring(0, timeString.length() - 2));
    }
}
