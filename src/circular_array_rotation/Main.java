/*
 * Copyright (C) 2017 Lamar J. Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package circular_array_rotation;

import static java.util.Collections.rotate;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author lsmith
 *
 * This is solution for a code challenge on hackerrank:
 * https://www.hackerrank.com/challenges/circular-array-rotation
 *
 * This class should be uploaded as a solution to the challenge. The package
 * declaration above should be removed as the submission engine does not support
 * them for uploaded answers. Alternatively, this code can just be copied and
 * pasted into the submission box, but take care to select the appropriate
 * language, Java 8 (or Java 7 in most cases), when submitting this way.
 */
public class Main {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int n = stdin.nextInt();
        int k = stdin.nextInt();
        int q = stdin.nextInt();
        
        LinkedList<Integer> numbers = getNumbers(stdin, n);        
        rotate(numbers, k);
        for (int m : getQueries(stdin, q)) {
            System.out.println(numbers.get(m));
        }                
    }

    private static LinkedList<Integer> getNumbers(Scanner stdin, int n) {
        LinkedList<Integer> numbers = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            numbers.add(stdin.nextInt());
        }
        return numbers;
    }

    private static int[] getQueries(Scanner stdin, int q) {
        int[] queries = new int[q];
        for (int i = 0; i < q; i++) {
            queries[i] =stdin.nextInt();
        }
        return queries;
    }
}
